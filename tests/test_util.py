#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020 - 2023 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for N23 Sensor utility functions.
"""

import typing as tp
from n23sensor.util import as_bool, as_button

import pytest

def test_as_bool() -> None:
    """
    Test conversion to bool.
    """
    assert as_bool('true')
    assert as_bool('True')
    assert as_bool('open')
    assert as_bool('ON')
    assert as_bool('on')
    assert as_bool('enabled')
    assert as_bool('1')

    assert not as_bool('abc')

@pytest.mark.parametrize(
    'value, expected',
    [[{'event': 'S'}, 1],
     [{'event': 'SS'}, 2],
     [{'event': 'SSS'}, 3],
     [{'event': 'L'}, -1],
     [1, 1],
     [2, 2],
     ['unknown-value', 0]])
def test_as_button(value: tp.Any, expected: int) -> None:
    """
    Test conversion to button value.
    """
    result = as_button(value)
    assert result == expected

# vim: sw=4:et:ai

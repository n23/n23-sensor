#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import hy  # noqa: F401
from pathlib import Path

import pytest

def pytest_collect_file(file_path: Path, parent: Path) -> pytest.Module | None:
    if file_path.name.startswith('test_') and file_path.suffix == '.hy':
        return pytest.Module.from_parent(parent, path=file_path)
    else:
        return None

# vim: sw=4:et:ai

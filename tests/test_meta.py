#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020 - 2023 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import hy
from n23sensor.types import Sensor
from n23sensor.meta import sensor_handle, as_sensor, sensor_list

import pytest

@pytest.mark.parametrize(
    'sensor, expected',
    [['pressure', 'pressure/attic'],
     ['battery-level', 'battery_level/attic'],
     [('switch', 'window'), 'switch/attic/window']])
def test_sensor_handle(sensor: str | tuple[str, str], expected: str) -> None:
    """
    Test creating sensor handle for N23 scheduler.
    """
    result = sensor_handle(sensor, 'attic')
    assert result == expected

def test_sensor_handle_values_error() -> None:
    """
    Test creating sensor handle with values for N23 scheduler.
    """
    with pytest.raises(ValueError, match='Switch sensor type missing'):
        sensor_handle('switch', 'attic')

def test_sensor_list() -> None:
    """
    Test getting sensor list.
    """
    sl = sensor_list()
    assert all(isinstance(s, hy.models.Symbol) for s in sl)


@pytest.mark.parametrize(
        'symbol, expected',
        [['pressure', Sensor('pressure', [])],
         [hy.models.Symbol('pressure'), Sensor('pressure', [])]])
def test_as_sensor(
        symbol: str | hy.models.Symbol | hy.models.Expression,
        expected: Sensor
) -> None:
    """
    Test converting sensor symbols to sensor classifier.
    """
    result = as_sensor(symbol)
    assert result == expected

# vim: sw=4:et:ai

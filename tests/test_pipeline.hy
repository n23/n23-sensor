;
; N23 Sensor - sensor data acquisition framework
;
; Copyright (C) 2020-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import operator :as op
        hy.pyops [< >]
        dataclasses :as dtc
        json

        n23
        n23.pipeline [identity fmap]
        n23sensor.pipeline [ctor-filter-time-increasing
                            ctor-filter-value
                            ctor-pipe-bluetooth
                            ctor-pipe-mqtt
                            transform-tuple-time-value
                            transform-nested-result]
        n23sensor.util [as-bool]
        pytest)

(require n23.pipeline [n23-> fmap-> ?->]
         n23sensor.pipeline [ctor-pipe-i2c-bme280])

(defn test-filter-time-increasing []
    #[[
    Test filter checking if time of sensor result is strictly increasing.
    ]]
    (setv f (ctor-filter-time-increasing))
    (setv tr (fn [ts val] (n23.TaskResult ts "handle" val)))
    (assert (= (f (tr 1 10)) (tr 1 10)))
    (assert (= (f (tr 2 10)) (tr 2 10)))
    (assert (is (f (tr 2 10)) None))
    (assert (= (f (tr 2.5 10)) (tr 2.5 10)))
    (assert (= (f (tr 2.2 10)) None))
    (assert (= (f (tr 2.3 10)) None))
    (assert (= (f (tr 2.6 10)) (tr 2.6 10))))

(defn [(pytest.mark.parametrize "sensor, value, expected"
        [['pressure 1000 True]
         ['pressure 0 False]
         ['pressure -5 False]
         ['humidity 15 True]
         ['humidity 0 False]
         ['humidity 100 True]
         ['humidity -1 False]
         ['humidity 101 False]])]
      test-filter-value [sensor value expected]

  (setv f (ctor-filter-value sensor))
  (setv result (f value))
  (assert (is result expected)))

(defn test-transform-tuple-time-value []
  (setv result (transform-tuple-time-value (n23.TaskResult
                                             1
                                             "handle"
                                             [11 "value"])))
  (assert (= result (n23.TaskResult 11 "handle" "value"))))

(defn test-transform-nested-value []
  (setv result (transform-nested-result (n23.TaskResult
                                          1
                                          "handle"
                                          {"time" 2
                                           "name" "nested"
                                           "value" "a-value"})))
  (assert (= result (n23.TaskResult 2 "nested" "a-value"))))

(defn test-pipe-i2c-bme280-ok []
  (setv pipe (ctor-pipe-i2c-bme280 "room"))
  (setv [r1 r2 r3] (pipe (n23.TaskResult 11 "handle" [10 12 15])))

  (assert (= r1.time 11))
  (assert (= r1.name "pressure/room"))
  (assert (= r1.value 10))

  (assert (= r2.time 11))
  (assert (= r2.name "temperature/room"))
  (assert (= r2.value 12))

  (assert (= r3.time 11))
  (assert (= r3.name "humidity/room"))
  (assert (= r3.value 15)))

(defn test-pipe-i2c-bme280-filtered []
  (setv pipe (ctor-pipe-i2c-bme280 "room"))
  (setv [r1 r2 r3] (pipe (n23.TaskResult 11 "multi" [0 12 0])))

  (assert (is r1 None))

  (assert (= r2.time 11))
  (assert (= r2.name "temperature/room"))
  (assert (= r2.value 12))

  (assert (is r3 None)))

(defn [(pytest.mark.parametrize "sensor, value, expected"
        [['pressure 1000 True]
         ['pressure 0 False]
         ['pressure -5 False]
         ['humidity 15 True]
         ['humidity 0 False]
         ['humidity 100 True]
         ['humidity -1 False]
         ['humidity 101 False]])]
      test-pipe-bluetooth [sensor value expected]

  (setv f (ctor-pipe-bluetooth sensor))
  (setv result (f (n23.TaskResult 1 sensor value)))
  (if expected
    (do
      (assert (= result.time 1))
      (assert (= result.name sensor))
      (assert (= result.value value)))
    (assert (is result None))))

; TODO: test for
;   ('button', (12.0, b'{"event": "SS"}'), 'json', 12.0, 2),
;   ('button', (13.0, b'{"event": "L"}'), 'json', 13.0, -1),
(defn test-pipe-mqtt []
  (setv f (ctor-pipe-mqtt "pressure" :parser json.loads))
  (setv result (f (n23.TaskResult 1 "pressure" #(11 b"2000"))))
  (assert (= result.time 11))
  (assert (= result.name "pressure"))
  (assert (= result.value 2000)))

(defn [(pytest.mark.parametrize "value, expected"
        [[b"off" False]
         [b"on" True]])]
      test-pipe-mqtt-switch [value expected]
  #[[
  Test MQTT pipeline for a switch sensor.
  ]]
  (setv f (ctor-pipe-mqtt "switch" :parser (n23-> bytes.decode as-bool)))
  (setv result (f (n23.TaskResult 1 "switch/window" #(11 value))))
  (assert (= result.time 11))
  (assert (= result.name "switch/window"))
  (assert (= result.value expected)))

; vim:sw=2:et:ai

#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Sensor and device metadata.
"""

import btzen
import hy.models  # type: ignore[import-untyped]
import logging
import typing as tp
from functools import cache

from .types import Sensor, SensorMeta, BluetoothSensorMeta

logger = logging.getLogger(__name__)

BLUETOOTH_SENSOR_LIST = {
    (btzen.Make.STANDARD, 'battery-level'):  BluetoothSensorMeta(btzen.battery_level),

    (btzen.Make.SENSOR_TAG, 'pressure'): BluetoothSensorMeta(btzen.pressure, regular=True),
    (btzen.Make.SENSOR_TAG, 'temperature'): BluetoothSensorMeta(btzen.temperature, regular=True),
    (btzen.Make.SENSOR_TAG, 'humidity'): BluetoothSensorMeta(btzen.humidity, regular=True),
    (btzen.Make.SENSOR_TAG, 'light'): BluetoothSensorMeta(btzen.light, regular=True),
    (btzen.Make.SENSOR_TAG, 'accelerometer'): BluetoothSensorMeta(btzen.accelerometer, interval=0.1),

    (btzen.Make.THINGY52, 'pressure'):  BluetoothSensorMeta(btzen.pressure),
    (btzen.Make.THINGY52, 'temperature'):  BluetoothSensorMeta(btzen.temperature),
    (btzen.Make.THINGY52, 'humidity'):  BluetoothSensorMeta(btzen.humidity),
}

SENSOR_LIST = {
    'pressure': SensorMeta('pressure', ['location', 'value']),
    'temperature': SensorMeta('temperature', ['location', 'value']),
    'humidity': SensorMeta('humidity', ['location', 'value']),
    'light': SensorMeta('light', ['location', 'value']),
    'accelerometer': SensorMeta('accelerometer', ['location', 'value']),
    'switch': SensorMeta('switch', ['location', 'type', 'value']),
    'button': SensorMeta('button', ['location', 'value']),
    'battery-level': SensorMeta('battery_level', ['location', 'value']),
}

def sensor_get(sensor: str) -> SensorMeta:
    """
    Get sensor metadata for sensor id.

    :param sensor: Sensor id.
    """
    return SENSOR_LIST[str(sensor)]

# Hylang has broken symbol class, so let's save ourselves with the
# function; see also https://github.com/hylang/hy/issues/2597
def as_sensor(sensor: tp.Any) -> Sensor:
    """
    Convert sensor to sensor classifier.

    Sensor can be one of

    - Python tuple or list
    - Hylang expression
    """
    logger.info('as sensor: {}, type={}'.format(sensor, type(sensor)))
    match sensor:
        case hy.models.Symbol() | str():
            result = Sensor(str(sensor), [])
        case hy.models.Expression() | tuple() | list():
            result = Sensor(str(sensor[0]), [str(v) for v in sensor[1:]])
        case _:
            raise ValueError(
                'Cannot convert sensor symbol to sensor classifier'
            )
    return result

def sensor_list() -> list[hy.models.Symbol]:
    return [hy.models.Symbol(v) for v in SENSOR_LIST]

@cache
def sensor_handle(sensor: tp.Any, location: str, any: bool=False) -> str:
    """
    Create handle for a sensor.
    """
    match as_sensor(sensor):
        case Sensor('switch', []) if (not any):
            raise ValueError('Switch sensor type missing')
        case Sensor('switch', [switch_type]):
            result = '{}/{}/{}'.format(
                sensor_get('switch').name, location, switch_type
            )
        case _ as s:
            result = '/'.join([sensor_get(s.name).name, location, *s.values])
    return result

# vim: sw=4:et:ai

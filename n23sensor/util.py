#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020 - 2023 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
N23 Sensor utility functions.
"""

import logging
import typing as tp

logger = logging.getLogger(__name__)

def as_bool(value: str) -> bool:
    """
    Convert string to a boolean value.
    """
    return value.lower() in ('true', 'open', 'on', 'enabled', '1')

def as_button(value: dict[tp.Any, tp.Any] | str | int) -> int:
    """
    Convert value representing button click to a button click integer.
    """
    match value:
        # support for shelly's button
        case {'event': 'S'}:
            clicks = 1
        case {'event': 'SS'}:
            clicks = 2
        case {'event': 'SSS'}:
            clicks = 3
        case {'event': 'L'}:
            clicks = -1
        # any integer is number of button clicks
        case int():
            clicks = value
        # any unknown value is
        case _:
            logger.warning(
                'cannot parse button value: {} ({})'.format(value, type(value))
            )
            clicks = 0

    return clicks

# vim: sw=4:et:ai

#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020-2025 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
RabbitMQ Streams sensor data dispatcher.
"""

import asyncio
import dataclasses as dtc
import logging
import n23
import rbfly.streams as rbs
import types
import typing as tp
from contextlib import AsyncExitStack
from collections.abc import AsyncGenerator
from functools import cache, partial
from n23.storage import Storage

from .types import SensorValue, ResultDict
from .meta import sensor_list, sensor_get

logger = logging.getLogger(__name__)

class StreamDispatcher(Storage):
    """
    RabbitMQ Streams sensor data dispatcher.

    It sends data to streams based on a sensor result name (handle).
    """
    def __init__(self, uri: str, prefix: str='n23-sensor.'):
        self._client = rbs.streams_client(uri)
        self._prefix = prefix
        self._publishers: dict[str, rbs.PublisherBatchFast] = {}
        self._stack_cm = AsyncExitStack()

    def add_entity(
            self,
            name: str,
            fields: tp.Sequence[str],
            *,
            batch: bool=False,
            max_len: int=1,
    ) -> None:
        pass

    async def write[SensorValue](self, result: n23.TaskResult[SensorValue]) -> None:
        """
        Send N23 task result to a RabbitMQ stream.

        :param result: N23 task result with sensor data.
        """
        if not self._publishers:
            logger.warning('no publishers defined yet')
            return

        sensor, _ = result.name.split('/', 1)

        data = tp.cast(rbs.AMQPBody, dtc.asdict(result))
        publisher = self._publishers[sensor]
        publisher.batch(data)

    async def flush(self) -> None:
        tasks = (p.flush() for p in self._publishers.values())
        await asyncio.gather(*tasks)

    async def __aenter__(self) -> None:
        client = self._client
        to_stream = partial('{}{}'.format, self._prefix)
        stack = await self._stack_cm.__aenter__()
        names = (sensor_get(s).name for s in sensor_list())
        self._publishers = {
            n: await stack.enter_async_context(client.publisher(
                    to_stream(n), cls=rbs.PublisherBatchFast
            ))
            for n in names
        }

    async def __aexit__(
        self,
        ex_type: type[BaseException] | None,
        ex: BaseException | None,
        traceback: types.TracebackType | None,
    ) -> None:
        try:
            await self._stack_cm.__aexit__(ex_type, ex, traceback)
            await asyncio.wait_for(self._client.disconnect(), timeout=30)
        except asyncio.TimeoutError as te:
            logger.warning('could not close streams client: %s', te)

@cache
def streams_client(uri: str) -> rbs.StreamsClient:
    return rbs.streams_client(uri)

@rbs.connection  # type: ignore
async def read_stream(
        client: rbs.StreamsClient, stream: str, /
) -> AsyncGenerator[ResultDict[SensorValue], None]:

    async for msg in client.subscribe(stream):
        yield tp.cast(ResultDict[SensorValue], msg)

@rbs.connection
async def read_stream_ts(
    client: rbs.StreamsClient, stream: str,
) -> AsyncGenerator[tuple[float, bytes], None]:

    async for msg in client.subscribe(stream):
        ts = rbs.get_message_ctx().stream_timestamp
        yield ts, tp.cast(bytes, msg)

# vim: sw=4:et:ai

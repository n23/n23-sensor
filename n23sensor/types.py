#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020 - 2023 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import dataclasses as dtc
import typing as tp

T = tp.TypeVar('T')
AccelerometerValue: tp.TypeAlias = tuple[float, float, float]
SensorValue = tp.TypeVar(
    'SensorValue',
    float, bool, int, AccelerometerValue
)

class ResultDict(tp.TypedDict, tp.Generic[T]):
    time: float
    name: str
    value: T

@dtc.dataclass(frozen=True)
class BluetoothSensorMeta:
    """
    Bluetooth sensor metadata.
    """
    ctor: tp.Callable[..., tp.Any]
    regular: bool = False
    interval: tp.Optional[float] = None  # by default use the one from config

@dtc.dataclass(frozen=True)
class Sensor:
    """
    Sensor classifier.

    Examples

        pressure sensor
            `Sensor('pressure')`
        window switch sensor
            `Sensor('switch', ['window'])`

    :var name: Sensor name.
    :var values: Sensor additional data.
    """
    name: str
    values: list[str]

@dtc.dataclass(frozen=True)
class SensorMeta:
    """
    Sensor metadata.

    Name of a sensor is

    - database table with sensor data
    - part of stream name defined in RabbitMQ Streams broker

    :var name: Name of a sensor.
    :var columns: Database columns for a sensor.
    """
    name: str  # also table
    columns: list[str]

# vim: sw=4:et:ai

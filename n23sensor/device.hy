;
; N23 Sensor - sensor data acquisition framework
;
; Copyright (C) 2020-2025 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import aiobme280   ; TODO: sensor specific, can we make it dynamic?
        aiotsl2591
        btzen

        contextvars [ContextVar]
        functools [partial]
        itertools [product]
        logging

        n23.app [n23-scheduler n23-add n23-sink-from-uri]
        n23sensor.app [bluetooth-device-add sink-get sink-list-adapter]
        n23sensor.meta [BLUETOOTH-SENSOR-LIST
                        sensor-get
                        sensor-handle
                        sensor-list]
        n23sensor.pipeline [ctor-pipe-bluetooth
                            ctor-pipe-mqtt
                            pipe-i2c-tsl2591
                            pipe-node]
        n23sensor.streams [read-stream read-stream-ts streams-client]
        n23sensor.sim [sim-i2c-bme280 sim-i2c-tsl2591])

(setv logger (logging.getLogger __name__))

(require n23.pipeline [n23-> fmap-> ?->]
         n23sensor.pipeline [ctor-pipe-i2c-bme280])

(defn sensor-i2c [cls device address location [interval 1]]
  ; TODO: sensor-handle seems to be not visible anymore at this stage; why?
  (import n23sensor.meta [sensor-handle])

  (setv read
    (match (str cls)
      "bme280" (. (aiobme280.Sensor device address) read)
      "tsl2591" (. (aiotsl2591.Sensor device address) read)
      "sim-bme280" sim-i2c-bme280
      "sim-tsl2591" sim-i2c-tsl2591
      _ (raise (ValueError (str.format "Unknown sensor class: {}" cls)))))

  (setv
    [handle pipe sink]
    (match (str cls)
      (| "bme280" "sim-bme280") [(str.format "multi/bme280-{}-0x{:2x}" device address)
                                 (ctor-pipe-i2c-bme280 location)
                                 (partial sink-list-adapter (sink-get))]
      (| "tsl2591" "sim-tsl2591") [(sensor-handle 'light location)
                                   pipe-i2c-tsl2591
                                   (sink-get)]
      _ (raise (ValueError (str.format "Unknown sensor class: {}" cls)))))

  (n23-add interval handle read pipe sink))

(defn sensor-bluetooth-* [cls address sensors location [interval 1]]
  (assert (isinstance location str))
  (for [sensor sensors]
    (sensor-bluetooth cls address sensor location :interval interval)))

(defn sensor-bluetooth [cls address sensor location [interval 1]]
  (setv make (btzen.Make (str.replace (str cls) "-" "_")))
  (setv sd (sensor-get sensor))
  (setv handle (sensor-handle sensor location))
  (setv meta (get BLUETOOTH-SENSOR-LIST #(make (str sensor))))
  (setv dev (meta.ctor address :make make))

  ; determine and use sensor interval
  (when (is-not meta.interval None) (setv interval meta.interval))
  (btzen.set-interval dev interval)

  (bluetooth-device-add dev)
  (if meta.regular
    (n23-add interval
             handle
             (partial bt-read dev)
             (ctor-pipe-bluetooth sensor)
             (sink-get))
    (n23-add handle
             (partial bt-read dev)
             (ctor-pipe-bluetooth sensor)
             (sink-get))))

(defn sensor-mqtt [uri topic sensor location [parser None]]
  (setv client (streams-client uri))
  (n23-add (sensor-handle sensor location)
           (partial read-stream-ts client topic)
           (ctor-pipe-mqtt sensor :parser parser)
           (sink-get)))

(defn sensor-mqtt-* [uri fmt-topic sensors locations [parser None]]
  (for [[sensor location] (product sensors locations)]
    (setv sd (sensor-get sensor))
    (sensor-mqtt uri
                 (fmt-topic.format :sensor sd.name :location location)
                 sensor
                 location
                 :parser parser)))

(defn sensor-node [uri [stream-prefix "n23-sensor"] [sensors None]]
  (when (is sensors None)
    (setv sensors (sensor-list)))

  (setv node-id (uri.replace "/" "-"))
  (setv client (streams-client uri))
  (for [sensor sensors]
    (setv sd (sensor-get sensor))
    (n23-add
      (sensor-handle sensor node-id :any True)
      (partial read-stream client f"{stream-prefix}.{sd.name}")
      pipe-node
      (sink-get))))

(defn :async bt-read [device]
  (try
    (await (btzen.read device))
    (except [ex [ConnectionError]]
      (logger.info(str.format "connection error: {}" ex)))))

; vim:sw=2:et:ai

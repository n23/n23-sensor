#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020-2025 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import hy  # type: ignore[import-untyped] # noqa: F401
from importlib.metadata import version

from .app import n23sensor_process, n23sensor_setup
from .meta import sensor_handle
from .device import sensor_bluetooth, sensor_mqtt, sensor_node, sensor_i2c # type: ignore[attr-defined]
from .device import hyx_sensor_bluetooth_XasteriskX, hyx_sensor_mqtt_XasteriskX  # type: ignore[attr-defined]

sensor_bluetooth_star = hyx_sensor_bluetooth_XasteriskX
sensor_mqtt_star = hyx_sensor_mqtt_XasteriskX

__version__ = version('n23-sensor')

__all__ = [
    'hyx_sensor_bluetooth_XasteriskX',
    'hyx_sensor_mqtt_XasteriskX',
    'n23sensor_process',
    'n23sensor_setup',
    'sensor_bluetooth',
    'sensor_bluetooth_star',
    'sensor_handle',
    'sensor_i2c',
    'sensor_mqtt',
    'sensor_mqtt_star',
    'sensor_node',
]

# vim: sw=4:et:ai

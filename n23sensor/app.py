#
# N23 Sensor - sensor data acquisition framework
#
# Copyright (C) 2020-2025 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
N23 Sensor application setup and processing.
"""

import btzen
import logging
import os
import typing as tp
from collections.abc import Awaitable, Sequence
from contextlib import AbstractAsyncContextManager
from contextvars import ContextVar
from functools import partial

import n23
from n23.app import n23_process, n23_scheduler, n23_sink_from_uri

from n23sensor.meta import SENSOR_LIST
from n23sensor.streams import StreamDispatcher
from n23sensor.types import SensorValue

logger = logging.getLogger(__name__)

type Process = AbstractAsyncContextManager[tp.Any]

type SinkFunction[SensorValue] = tp.Callable[
    [n23.TaskResult[SensorValue]], Awaitable[tp.Any]
]

CTX_BT_DEVICE = ContextVar('CTX_BT_DEVICE', default=set())  # type: ignore
CTX_SINK = ContextVar[SinkFunction[tp.Any]]('CTX_SINK')
CTX_PROCESS = ContextVar[set[Process]]('CTX_PROCESS', default=set[Process]())

def n23sensor_setup(
        stream_broker: str,
        db: str | None=None,
) -> None:

    scheduler = n23_scheduler()
    scheduler.timeout = 0.75

    streams = StreamDispatcher(stream_broker)
    storage = n23.StorageAbyss() if db is None else n23_sink_from_uri(db)

    async def sink(value: n23.TaskResult[SensorValue]) -> None:
        await streams.write(value)
        await storage.write(value)

    add_entity = partial(storage.add_entity, batch=True)
    for sd in SENSOR_LIST.values():
        add_entity(sd.name, sd.columns)

    if os.environ.get('N23_SENSOR_SINK_DEBUG') == '1':
        sink = log_task_result(sink)  # type: ignore

    scheduler.add(1, 'db-flush', storage.flush)
    scheduler.add(1, 'stream-flush', streams.flush)
    CTX_SINK.set(sink)
    CTX_PROCESS.get().update([streams, storage])

def log_task_result(
        sink: SinkFunction[SensorValue]
) -> SinkFunction[SensorValue]:

    async def _sink(result: n23.TaskResult[SensorValue]) -> None:
        logger.info(
            'sink received task result: {}, type={}'
            .format(result, type(result))
        )
        await sink(result)
    return _sink

async def sink_list_adapter(
        sink: SinkFunction[SensorValue],
        data: Sequence[n23.TaskResult[SensorValue]],
) -> None:
    """
    Send task result or a collection of task results of N23 scheduler to
    a sink.
    """
    for v in filter(None, data):
        await sink(v)

def n23sensor_process(*args: Process) -> None:
    internal = list(CTX_PROCESS.get())
    bt_devices = CTX_BT_DEVICE.get()
    if bt_devices:
        internal.append(btzen.connect(bt_devices))
    n23_process(*internal, *args)

def bluetooth_device_add[T](dev: btzen.Device[btzen.Service, T]) -> None:
    CTX_BT_DEVICE.get().add(dev)

def sink_get() -> SinkFunction[SensorValue]:
    return CTX_SINK.get()

# vim:sw=4:et:ai

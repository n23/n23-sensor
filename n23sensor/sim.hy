;
; N23 Sensor - sensor data acquisition framework
;
; Copyright (C) 2020-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

"""
Simulators for N23 Sensor application.
"""

(import time)

(defn :async sim-i2c-bme280 []
  (while True
    (yield [10 11 12])))

(defn :async sim-i2c-tsl2591 []
  (while True
    (yield 91)))

; vim:sw=2:et:ai

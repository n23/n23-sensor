;
; N23 Sensor - sensor data acquisition framework
;
; Copyright (C) 2020-2025 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

"""
N23 pipeline filter - function returning result or null.
Filter function - function return bool.
"""

(import dataclasses :as dtc
        functools [partial]
        json
        operator :as op
        n23
        n23.pipeline [identity]
        n23sensor.meta [as-sensor sensor-handle]
        n23sensor.types [Sensor])

(require n23.pipeline [n23-> fmap-> ?->])

(setv result-to-tuple (op.itemgetter "time" "name" "value"))

;
; filters
;
(setv filter-pressure (partial op.lt 0))
(setv filter-humidity (fn [v] (chainc 0 < v <= 100)))

(defn ctor-filter-time-increasing []
  #[[
  Create N23 pipeline filter to check if time of a sensor result is
  strictly increasing.
  ]]
  (let [ts 0]
    (fn [result]
      (nonlocal ts)
      (if (< ts result.time) (do (setv ts result.time) result) None))))

(defn ctor-filter-value [sensor]
  #[[
  Create filter function for a sensor

  .. seealso::
     - filter-pressure
     - filter-humidity
  ]]
  (match (as-sensor sensor)
    (Sensor "pressure") filter-pressure
    (Sensor "humidity") filter-humidity
    _ (fn [v] True)))

;
; transformations
;
(defn transform-tuple-time-value [result]
  (setv [ts v] result.value)
  (dtc.replace result :time ts :value v))

(defn transform-nested-result [result]
  (n23.TaskResult #* (result-to-tuple result.value)))

;
; pipelines
;
(setv pipe-node transform-nested-result)
(setv pipe-i2c-tsl2591 identity)

(defmacro ctor-pipe-i2c-bme280 [location]
  `(do 
     (setv sensor-handle hy.I.n23sensor.meta.sensor-handle)
     (setv pl hy.I.n23sensor.pipeline)

     (setv pressure-name (sensor-handle 'pressure ~location))
     (setv temperature-name (sensor-handle 'temperature ~location))
     (setv humidity-name (sensor-handle 'humidity ~location))

     (n23-> [(pressure-name (fmap-> ?-> pl.filter-pressure))
             (temperature-name pl.identity)
             (humidity-name (fmap-> ?-> pl.filter-humidity))])))

(defn ctor-pipe-bluetooth [sensor]
  (setv filter-value (ctor-filter-value sensor))
  (n23-> (fmap-> ?-> bool) (fmap-> ?-> filter-value)))

(defn ctor-pipe-mqtt [sensor [parser None]]
  (when (is parser None) (setv parser bytes.decode))
  (setv filter-time (ctor-filter-time-increasing))
  (setv filter-value (ctor-filter-value sensor))
 
  (n23-> transform-tuple-time-value
         filter-time
         (fmap-> try-parse-value parser)
         (fmap-> ?-> filter-value)))

(defn try-parse-value [data parser]
  (try
    (parser data)
    (except [ex Exception]
    (raise (ValueError
             (str.format "Cannot parse value: {!r}, type={}" data (type data)))
           :from ex))))

; vim:sw=2:et:ai
